package org.bruno.vote.session.model

import javax.persistence.*

@Entity
data class Vote(
        @Id
        @Column
        @GeneratedValue(strategy= GenerationType.SEQUENCE)
        val id: Long? = null,

        @Column
        val cpf: String,

        @Column
        @Enumerated(EnumType.STRING)
        val response: VoteResponse,

        @JoinColumn
        @OneToOne
        val guideline: Guideline) {

    override fun toString(): String {
        return "Vote(id=$id, cpf='$cpf', response=$response)"
    }
}

enum class VoteResponse {
    YES, NO;

    companion object {
        fun of(vote: String): VoteResponse {
            return when (vote) {
                "YES" -> YES
                "NO" -> NO
                else -> throw VoteResponseNotValidException(vote)
            }
        }
    }
}

class VoteResponseNotValidException(vote: String) : Exception("$vote is not a valid vote, please use one of '${VoteResponse.values().joinToString()}'")