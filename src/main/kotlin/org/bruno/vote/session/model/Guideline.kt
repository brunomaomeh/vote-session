package org.bruno.vote.session.model

import org.bruno.vote.session.loggin.logger
import javax.persistence.*

@Entity
data class Guideline(
        @Id
        @Column
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        val id: Long? = null,

        @Column
        val description: String,

        @Column
        var active: Boolean = false,

        @OneToMany(mappedBy = "guideline", fetch = FetchType.EAGER)
        val votes: List<Vote> = emptyList()) {

    fun close(): Guideline = this.copy(active = false)

    fun results() = if (active) {
        throw UnfinishedVoteSessionException(id)
    } else {
        collectResults()
    }

    private fun collectResults() = VoteResults(description, votes)

    data class VoteResults private constructor(val guideline: String, val agree: List<Vote>, val disagree: List<Vote>) {

        constructor(description: String, allVotes: List<Vote>) : this(
                guideline = description,
                agree = allVotes.filter { it.response == VoteResponse.YES },
                disagree = allVotes.filter { it.response == VoteResponse.NO }
        )

        fun mostVoted() = when {
            agree.size > disagree.size -> "agree"
            agree.size < disagree.size -> "disagree"
            else -> "both"
        }
    }
}

data class UnfinishedVoteSessionException(val id: Long?) :
        Exception(id?.let { "The session vote $id is not ended!" } ?: "There is no vote session!!")
