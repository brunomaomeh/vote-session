package org.bruno.vote.session.config.jms

import org.apache.activemq.ActiveMQConnectionFactory
import org.bruno.vote.session.loggin.logger
import org.bruno.vote.session.service.vote.ActiveMQBroadcastService
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jms.annotation.EnableJms
import org.springframework.jms.config.DefaultJmsListenerContainerFactory
import org.springframework.jms.config.JmsListenerContainerFactory
import org.springframework.jms.core.JmsTemplate
import javax.annotation.PostConstruct
import javax.jms.ConnectionFactory
import javax.jms.Session

@EnableJms
@Configuration
class JmsConfig(private val jmsProperties: JmsProperties) {
    private val logger = logger<JmsConfig>()

    @PostConstruct
    fun setup() {
        val connectionFactory = ActiveMQConnectionFactory(jmsProperties.brokerUrl)

        val connection = connectionFactory.createConnection()
        connection.start()

        val session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)

        session.createQueue(ActiveMQBroadcastService.VOTE_SESSION_QUEUE)
        logger.debug("Created a ${ActiveMQBroadcastService.VOTE_SESSION_QUEUE} queue")
    }

    @Bean
    fun connectionFactory(): ActiveMQConnectionFactory {
        return if ("" == jmsProperties.user) {
            ActiveMQConnectionFactory(jmsProperties.brokerUrl)
        } else ActiveMQConnectionFactory(jmsProperties.user, jmsProperties.password, jmsProperties.brokerUrl)
    }

    @Bean
    fun jmsFactoryTopic(connectionFactory: ConnectionFactory,
                        configurer: DefaultJmsListenerContainerFactoryConfigurer): JmsListenerContainerFactory<*> {
        val factory = DefaultJmsListenerContainerFactory()
        configurer.configure(factory, connectionFactory)
        return factory
    }

    @Bean
    fun jmsTemplate() = JmsTemplate(connectionFactory())

    @Bean
    fun jmsTemplateTopic() = JmsTemplate(connectionFactory())
}

@Configuration
class JmsProperties(
    @Value("\${spring.activemq.broker-url}")
    val brokerUrl: String,

    @Value("\${spring.activemq.user}")
    val user: String,

    @Value("\${spring.activemq.password}")
    val password: String
)