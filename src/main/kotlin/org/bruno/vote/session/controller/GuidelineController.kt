package org.bruno.vote.session.controller

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.bruno.vote.session.model.Guideline
import org.bruno.vote.session.model.Vote
import org.bruno.vote.session.service.GuidelineService
import org.bruno.vote.session.service.vote.VoteService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/guidelines")
@Api(description = "Operações persistentes à Pautas e Sessão de votos.")
class GuidelineController(private val guidelineService: GuidelineService,
                          private val voteService: VoteService) {

    @PostMapping("")
    @ApiOperation("Cadastra uma nova pauta.")
    fun new(@RequestBody guidelineRequest: GuidelineRequest): GuidelineResponse {
        val guideline = guidelineRequest.toGuideline()
        val newGuideline = guidelineService.save(guideline)
        return GuidelineResponse(newGuideline)
    }

    @PutMapping("/{id}")
    @ApiOperation("Abre uma sessão para votação")
    fun openVotes(@PathVariable id: Long,
                  @RequestBody voteSessionRequest: VoteSessionRequest): VoteSessionResponse {
        val timeout = voteSessionRequest.timeout
        val guideline = guidelineService.openVotes(id, timeout)
        return VoteSessionResponse(guideline, timeout)
    }

    @PostMapping("/{id}/votes")
    @ApiOperation("Vota em uma pauta")
    fun newVote(@PathVariable id: Long,
                @RequestBody voteRequest: VoteRequest): VoteResponse {
        val newVote = voteService.vote(id, voteRequest)

        return VoteResponse(newVote)
    }

    @GetMapping("{id}/votes")
    @ApiOperation("Visualiza os votos contabilizados de uma pauta")
    fun results(@PathVariable id: Long): VoteResultResponse {
        val results = voteService.results(id)

        return VoteResultResponse(results)
    }

    data class GuidelineRequest(val description: String) {
        fun toGuideline() = Guideline(description = description)
    }
    data class GuidelineResponse(val id: Long?, val description: String) {
        constructor(guideline: Guideline) : this(
                id = guideline.id,
                description = guideline.description
        )
    }

    data class VoteSessionRequest(val timeout: Long = 60)
    data class VoteSessionResponse(val guideline: Long?, val timeout: Long) {
        constructor(guideline: Guideline, timeout: Long) : this(
                guideline = guideline.id,
                timeout = timeout
        )
    }

    data class VoteRequest(val cpf: String,
                           val vote: String,
                           val guideline: Long) {
        fun toVote(guideline: Guideline): Vote {
            return Vote(cpf = cpf,
                    response = org.bruno.vote.session.model.VoteResponse.of(vote),
                    guideline = guideline)
        }
    }

    data class VoteResponse(val id: Long,
                            val cpf: String,
                            val vote: String) {
        constructor(vote: Vote) : this(vote.id!!,
                vote.cpf,
                vote.response.name)
    }

    data class VoteResultResponse(val guideline: String,
                                  val totalAgreement: Int,
                                  val totalDisagreement: Int,
                                  val summary: String) {
        constructor(voteResults: Guideline.VoteResults) : this(
                guideline = voteResults.guideline,
                totalAgreement = voteResults.agree.size,
                totalDisagreement = voteResults.disagree.size,
                summary = "The most voted is: ${voteResults.mostVoted()}"
        )
    }

}

