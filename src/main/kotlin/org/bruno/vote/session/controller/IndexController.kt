package org.bruno.vote.session.controller

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/")
@Api(description = "Apenas um 'hello world' para ver as configurações funcionando.")
class IndexController {

    @GetMapping("")
    @ApiOperation("Retorna 'hello world'")
    fun index() = "hello world"

}