package org.bruno.vote.session.repository

import org.bruno.vote.session.model.Guideline
import org.springframework.data.repository.CrudRepository

interface GuidelineRepository : CrudRepository<Guideline, Long> {
    fun findByIdAndActive(guidelineId: Long, active: Boolean): Guideline?
}