package org.bruno.vote.session.repository

import org.bruno.vote.session.model.Guideline
import org.bruno.vote.session.model.Vote
import org.springframework.data.repository.CrudRepository

interface VoteRepository : CrudRepository<Vote, Long> {
    fun findByCpfAndGuideline(cpf: String, guideline: Guideline) : List<Vote>
}