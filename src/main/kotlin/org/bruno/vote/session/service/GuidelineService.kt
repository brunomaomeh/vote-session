package org.bruno.vote.session.service

import org.bruno.vote.session.extenssions.unwrap
import org.bruno.vote.session.loggin.logger
import org.bruno.vote.session.model.Guideline
import org.bruno.vote.session.repository.GuidelineRepository
import org.bruno.vote.session.service.vote.VoteBroadcastService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.ResponseStatus
import reactor.core.publisher.Mono
import java.time.Duration
import java.time.temporal.ChronoUnit
import javax.transaction.Transactional

@Service
class GuidelineService(private val guidelineRepository: GuidelineRepository,
                       private val voteBroadcastService: List<VoteBroadcastService>) {
    private val logger = logger<GuidelineService>()
    fun save(guideline: Guideline): Guideline {
        logger.debug("Saving a new guideline: $guideline")
        return guidelineRepository.save(guideline)
    }

    fun openVotes(guidelineId: Long, timeout: Long): Guideline {
        val guideline = guidelineRepository.findByIdAndActive(guidelineId, false)
        return if (guideline != null) {
            logger.debug("Now the $guideline is able to be voted")
            val updateableGuideline = guideline.copy(active = true)
            guidelineRepository.save(updateableGuideline)
            updateableGuideline.closeAt(timeout)
            updateableGuideline
        } else {
            throw GuidelineNotFoundException(guidelineId)
        }
    }

    private fun Guideline.closeAt(timeout: Long) {
        Mono.just(this)
                .delayElement(Duration.of(timeout, ChronoUnit.SECONDS))
                .subscribe {
                    close(it.id!!)
                }
    }

    @Transactional
    fun close(guidelineId: Long) {
        val guideline = guidelineRepository.findById(guidelineId).unwrap()
        if (guideline != null) {
            logger.debug("The $guideline is closed!")
            val closedGuideline = guideline.close()
            guidelineRepository.save(closedGuideline)
            broadcast(closedGuideline)
        } else {
            throw GuidelineNotFoundException(guidelineId)
        }
    }

    private fun broadcast(guideline: Guideline) {
        voteBroadcastService.forEach{ it.send(guideline) }
    }

}

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class GuidelineNotFoundException(id: Long) :
        Exception("The guideline $id was not found!")
