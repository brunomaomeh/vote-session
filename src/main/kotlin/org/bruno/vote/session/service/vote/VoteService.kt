package org.bruno.vote.session.service.vote

import org.bruno.vote.session.controller.GuidelineController
import org.bruno.vote.session.extenssions.unwrap
import org.bruno.vote.session.loggin.logger
import org.bruno.vote.session.model.Guideline
import org.bruno.vote.session.model.Vote
import org.bruno.vote.session.repository.GuidelineRepository
import org.bruno.vote.session.repository.VoteRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.ResponseStatus

@Service
class VoteService(private val guidelineRepository: GuidelineRepository,
                  private val voteRepository: VoteRepository,
                  private val validations: List<VoteValidation>) {
    private val logger = logger<VoteService>()

    fun vote(guidelineId: Long, voteRequest: GuidelineController.VoteRequest): Vote {
        val guideline = guidelineRepository
                .findByIdAndActive(guidelineId, true)

        return if (guideline != null) {
            val vote = voteRequest.toVote(guideline)
            vote.validate()
            logger.debug("A new vote was accounted for: $vote")
            voteRepository.save(vote)
        } else {
            throw VoteSessionNotFoundException(guidelineId)
        }
    }

    private fun Vote.validate() {
        val valid = validations.filter{
            it.isNotValid(this)
        }
        if (valid.isNotEmpty()) {
            throw VoteValidationException(this, valid)
        }
    }

    fun results(guidelineId: Long): Guideline.VoteResults {
        val guideline = guidelineRepository.findById(guidelineId).unwrap()
        return guideline?.results() ?: throw VoteSessionNotFoundException(guidelineId)
    }

}

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
class VoteSessionNotFoundException(guidelineId: Long) :
        Exception("The vote session is closed for the guideline $guidelineId")

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
class VoteValidationException(vote: Vote, valid: List<VoteValidation>) :
        Exception(valid.joinToString { it.message(vote) })
