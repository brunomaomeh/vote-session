package org.bruno.vote.session.service.vote

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import org.bruno.vote.session.controller.GuidelineController
import org.bruno.vote.session.loggin.logger
import org.bruno.vote.session.model.Guideline
import org.springframework.jms.core.JmsTemplate
import org.springframework.stereotype.Component


interface VoteBroadcastService{
    fun send(guideline: Guideline)
}


@Component
class ActiveMQBroadcastService(private val jmsTemplate: JmsTemplate) : VoteBroadcastService {
    private val logger = logger<ActiveMQBroadcastService>()

    companion object {
        const val VOTE_SESSION_QUEUE = "vote-session-queue"
    }

    override fun send(guideline: Guideline) {
        logger.debug("Sending the $guideline to queue")
        val mapper = XmlMapper()
        jmsTemplate.convertAndSend(VOTE_SESSION_QUEUE,
                mapper.writeValueAsString(GuidelineController.VoteResultResponse(guideline.results())))
    }
}