package org.bruno.vote.session.service.vote

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.jackson.responseObject
import com.github.kittinunf.result.Result
import org.bruno.vote.session.loggin.logger
import org.bruno.vote.session.model.Vote
import org.bruno.vote.session.repository.VoteRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.ResponseStatus

interface VoteValidation {
    fun isNotValid(vote: Vote): Boolean
    fun message(vote: Vote): String
}

@Component
class UniqueVoteValidation(private val voteRepository: VoteRepository) : VoteValidation {
    private val logger = logger<UniqueVoteValidation>()

    override fun isNotValid(vote: Vote): Boolean {
        val found = voteRepository.findByCpfAndGuideline(vote.cpf, vote.guideline)
        logger.debug("This is a unique vote? ${found.isEmpty()}")

        return found.isNotEmpty()
    }

    override fun message(vote: Vote) = "This cpf ${vote.cpf} already voted"
}

@Component
class CPFCanVoteValidation : VoteValidation {
    companion object {
        private const val VALIDATION_URL = "https://user-info.herokuapp.com/users/"
        const val UNABLE_TO_VOTE = "UNABLE_TO_VOTE"
    }

    override fun isNotValid(vote: Vote): Boolean {
        val url = appendUrl(vote)
        return makeRequest(url, vote)
    }

    private fun appendUrl(vote: Vote): String {
        return buildString {
            append(VALIDATION_URL)
            append(vote.cpf)
        }
    }

    private fun makeRequest(url: String, vote: Vote): Boolean {
        val (_, _, result) =
                url.httpGet().responseObject<ExternalCPF>()

        return when (result) {
            is Result.Failure -> throw InvalidCPFException(vote.cpf, result.error.exception)
            is Result.Success -> result.value.status == UNABLE_TO_VOTE
        }
    }

    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    class InvalidCPFException(cpf: String, t: Throwable) : Exception("Use a valid cpf: $cpf", t)

    override fun message(vote: Vote) = "This cpf ${vote.cpf} is not able to vote!"

    data class ExternalCPF(val status: String)
}