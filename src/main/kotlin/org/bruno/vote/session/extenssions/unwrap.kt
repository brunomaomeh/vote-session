package org.bruno.vote.session.extenssions
import java.util.*

fun <T> Optional<T>.unwrap(): T? = this.orElse(null)
