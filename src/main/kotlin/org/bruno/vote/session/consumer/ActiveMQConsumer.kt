package org.bruno.vote.session.consumer

import org.bruno.vote.session.loggin.logger
import org.springframework.jms.annotation.JmsListener
import org.springframework.stereotype.Service

@Service
class ActiveMQConsumer {
    private val logger = logger<ActiveMQConsumer>()

    @JmsListener(destination = "vote-session-queue")
    fun onReceiverQueue(str: String) {
        logger.info(str)
    }


}