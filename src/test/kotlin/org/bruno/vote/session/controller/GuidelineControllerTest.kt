package org.bruno.vote.session.controller

import io.kotlintest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import org.bruno.vote.session.model.Guideline
import org.bruno.vote.session.model.Vote
import org.bruno.vote.session.model.VoteResponse
import org.bruno.vote.session.service.GuidelineService
import org.bruno.vote.session.service.vote.VoteService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class GuidelineControllerTest {
    private val guidelineService = mockk<GuidelineService>()
    private val voteService = mockk<VoteService>()

    private lateinit var guidelineController: GuidelineController

    @BeforeEach
    fun setup() {
        guidelineController = GuidelineController(guidelineService, voteService)
    }

    @Test
    fun `should save a new guideline`() {
        every { guidelineService.save(any()) } returns Guideline(1, "guideline")

        val guidelineRequest =
                GuidelineController.GuidelineRequest("guideline")
        val response = guidelineController.new(guidelineRequest)

        response shouldBe GuidelineController.GuidelineResponse(1, "guideline")
    }

    @Test
    fun `should open a new vote session`() {
        val guideline = Guideline(id = 10, description = "guideline")
        every { guidelineService.openVotes(any(), any()) } returns guideline
        val request = GuidelineController.VoteSessionRequest(5)
        val response = guidelineController.openVotes(10, request)

        response shouldBe GuidelineController.VoteSessionResponse(10, 5)
    }

    @Test
    fun `it should vote`() {
        val guideline = Guideline(id = 10, description = "guideline")
        val voteRequest = GuidelineController.VoteRequest("000", "YES", 10)
        every { voteService.vote(10, voteRequest) } returns
                Vote(30, "000", VoteResponse.YES, guideline)
        val newVote = guidelineController.newVote(10, voteRequest)
        newVote shouldBe GuidelineController.VoteResponse(30, "000", "YES")
    }
    
    @Test
    fun `it should retrieve the vote results`() {
        val guideline = Guideline(1, "guideline")

        val votes = listOf(
                Vote(1, "12312312311", VoteResponse.YES, guideline),
                Vote(2, "12312312322", VoteResponse.YES, guideline),
                Vote(3, "12312312333", VoteResponse.NO, guideline)
        )

        val voteResults = Guideline.VoteResults("guideline", votes)
        every { voteService.results(any()) } returns voteResults
        val results = guidelineController.results(10)

        results shouldBe GuidelineController.VoteResultResponse(
                "guideline",
                totalAgreement = 2,
                totalDisagreement = 1,
                summary = "The most voted is: agree"
        )
    }
}