package org.bruno.vote.session.controller

import io.kotlintest.matchers.shouldBe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class IndexControllerTest {

    lateinit var indexController: IndexController

    @BeforeEach
    fun setup() {
        indexController = IndexController()
    }

    @Test
    fun `it should return a hello world string`() {
        val hello = indexController.index()

        hello shouldBe "hello world"
    }
}