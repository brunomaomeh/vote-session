package org.bruno.vote.session.model

import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import org.junit.jupiter.api.Test

internal class VoteResponseTest {
    @Test
    fun `it should return YES`() {
        VoteResponse.of("YES") shouldBe VoteResponse.YES
    }

    @Test
    fun `it should return NO`() {
        VoteResponse.of("NO") shouldBe VoteResponse.NO
    }

    @Test
    fun `it should throw an VoteResponseNotValidException`() {
        val exception = shouldThrow<VoteResponseNotValidException> {
            VoteResponse.of("any")
        }

        exception.message shouldBe "any is not a valid vote, please use one of '${VoteResponse.values().joinToString()}'"
    }

}