package org.bruno.vote.session.model

import io.kotlintest.matchers.shouldBe
import org.bruno.vote.session.model.VoteResponse.NO
import org.bruno.vote.session.model.VoteResponse.YES
import org.junit.jupiter.api.Test

internal class VoteResultsTest {

    @Test
    fun `it should split the votes into two arrays, agree and disagree`() {
        val guideline = Guideline(1, "guideline")
        val votes = listOf(
                Vote(1, "12312312311", YES, guideline),
                Vote(2, "12312312322", YES, guideline),
                Vote(3, "12312312333", NO, guideline),
                Vote(4, "12312312344", NO, guideline)
        )

        val agree = listOf(
                Vote(1, "12312312311", YES, guideline),
                Vote(2, "12312312322", YES, guideline)
        )
        val disagree = listOf(
                Vote(3, "12312312333", NO, guideline),
                Vote(4, "12312312344", NO, guideline)
        )

        val results = Guideline.VoteResults("guideline", votes)
        results.agree shouldBe agree
        results.disagree shouldBe disagree
    }

    @Test
    fun `agree should be the most voted`() {
        val guideline = Guideline(1, "guideline")

        val votes = listOf(
                Vote(1, "12312312311", YES, guideline),
                Vote(2, "12312312322", YES, guideline),
                Vote(3, "12312312333", NO, guideline)
        )

        Guideline.VoteResults("guideline", votes).mostVoted() shouldBe "agree"
    }

    @Test
    fun `disagree should be the most voted`() {
        val guideline = Guideline(1, "guideline")

        val votes = listOf(
                Vote(1, "12312312311", YES, guideline),
                Vote(2, "12312312322", NO, guideline),
                Vote(3, "12312312333", NO, guideline)
        )

        Guideline.VoteResults("guideline", votes).mostVoted() shouldBe "disagree"
    }

    @Test
    fun `both should have the same amount of votes`() {
        val guideline = Guideline(1, "guideline")

        val votes = listOf(
                Vote(1, "12312312311", YES, guideline),
                Vote(2, "12312312322", YES, guideline),
                Vote(3, "12312312333", NO, guideline),
                Vote(4, "12312312344", NO, guideline)
        )

        Guideline.VoteResults("guideline", votes).mostVoted() shouldBe "both"
    }

}