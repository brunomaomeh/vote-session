package org.bruno.vote.session.model

import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import org.junit.jupiter.api.Test

internal class GuidelineTest {
    @Test
    fun `it should close a guideline`() {
        val guideline = Guideline(id = 1, description = "Guideline", active = true)
        val closed = guideline.close()

        closed shouldBe Guideline(id = 1, description = "Guideline", active = false)
    }

    @Test
    fun `it should retrieve the results of a guideline`() {
        val guideline = Guideline(id = 1, description = "guideline", active = true)

        val votes = listOf(
                Vote(1, "001", VoteResponse.YES, guideline),
                Vote(2, "002", VoteResponse.YES, guideline),
                Vote(3, "003", VoteResponse.NO, guideline)
        )

        val results = guideline.copy(votes = votes).close().results()

        results shouldBe Guideline.VoteResults("guideline", votes)
    }

    @Test
    fun `it should throw an UnfinishedVoteSessionException while getting the results of an unclosed guideline`() {
        val guideline = Guideline(id = 1, description = "guideline", active = true)

        val votes = listOf(
                Vote(1, "001", VoteResponse.YES, guideline),
                Vote(2, "002", VoteResponse.YES, guideline),
                Vote(3, "003", VoteResponse.NO, guideline)
        )


        val results = shouldThrow<UnfinishedVoteSessionException> {
            guideline.copy(votes = votes).results()
        }

        results.message shouldBe "The session vote 1 is not ended!"
    }
}