package org.bruno.vote.session.service.vote

import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.bruno.vote.session.controller.GuidelineController
import org.bruno.vote.session.model.Guideline
import org.bruno.vote.session.model.Vote
import org.bruno.vote.session.model.VoteResponse
import org.bruno.vote.session.repository.GuidelineRepository
import org.bruno.vote.session.repository.VoteRepository
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*

internal class VoteServiceTest {
    private val guidelineRepository = mockk<GuidelineRepository>()
    private val voteRepository = mockk<VoteRepository>()
    private val voteValidation = mockk<VoteValidation>()
    private val validation = listOf(voteValidation)

    private lateinit var voteService: VoteService

    @BeforeEach
    fun setup() {
        voteService = VoteService(guidelineRepository, voteRepository, validation)
    }

    @Test
    fun `it should throw a GuidelineNotFoundException when there is no Guideline Or Guideline`() {
        every { guidelineRepository.findByIdAndActive(any(), any()) } returns null

        val voteRequest = GuidelineController.VoteRequest( "000", "YES", 1)
        val exception = shouldThrow<VoteSessionNotFoundException> {
            voteService.vote(10, voteRequest)
        }

        exception.message shouldBe "The vote session is closed for the guideline 10"
    }

    @Test
    fun `it should thrown a VoteException if there is any validation problem`() {
        val guideline = mockk<Guideline>()
        every { guidelineRepository.findByIdAndActive(any(), any()) } returns guideline

        every { voteValidation.isNotValid(any()) } returns true
        every { voteValidation.message(any()) } returns "Test error vote validation"
        val voteRequest = GuidelineController.VoteRequest("000", "YES", 1)
        val exception = shouldThrow<VoteValidationException> {
            voteService.vote(10, voteRequest)
        }

        exception.message shouldBe "Test error vote validation"
    }

    @Test
    fun `it should save a new vote if there is no validation errors`() {
        val guideline = mockk<Guideline>()
        val vote = Vote(40, "000", VoteResponse.YES, guideline)
        every { guidelineRepository.findByIdAndActive(any(), any()) } returns guideline
        every { voteValidation.isNotValid(any()) } returns false
        every { voteRepository.save(any<Vote>()) } returns vote

        val voteRequest = GuidelineController.VoteRequest("000", "YES", 1)
        val newVote = voteService.vote(10, voteRequest)

        newVote shouldBe vote
        verify { voteValidation.isNotValid(any()) }
    }

    @Test
    fun `it should retrieve the votes result`() {
        val guideline = mockk<Guideline>()

        val votes = listOf(
                Vote(1, "12312312311", VoteResponse.YES, guideline),
                Vote(2, "12312312322", VoteResponse.YES, guideline),
                Vote(3, "12312312333", VoteResponse.NO, guideline)
        )
        every { guidelineRepository.findById(any()) } returns Optional.of(guideline)
        every { guideline.results() } returns Guideline.VoteResults("guideline", votes)

        val results = voteService.results(10)

        results shouldBe Guideline.VoteResults("guideline", votes)
    }

    @Test
    fun `'results' - it should throw a GuidelineNotFoundException when there is no VoteSession Or Guideline`() {
        every { guidelineRepository.findById(any()) } returns Optional.empty()

        val exception = shouldThrow<VoteSessionNotFoundException> {
            voteService.results(10)
        }

        exception.message shouldBe "The vote session is closed for the guideline 10"
    }


}