package org.bruno.vote.session.service.vote

import com.github.kittinunf.fuel.core.Client
import com.github.kittinunf.fuel.core.FuelManager
import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import io.mockk.every
import io.mockk.mockk
import org.bruno.vote.session.model.Guideline
import org.bruno.vote.session.model.Vote
import org.bruno.vote.session.model.VoteResponse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class CPFCanVoteValidationTest {

    private val client = mockk<Client>()
    private lateinit var cpfCanVoteValidation: CPFCanVoteValidation

    @BeforeEach
    fun setup() {
        FuelManager.instance.client = client
        cpfCanVoteValidation = CPFCanVoteValidation()
    }

    @Test
    fun `it should throw a InvalidCPFException if cpf is not valid`() {
        every { client.executeRequest(any()).statusCode } returns 404
        every { client.executeRequest(any()).responseMessage } returns "ERROR"
        every { client.executeRequest(any()).data } returns "Not found".toByteArray()

        val guideline = Guideline(1, "Guideline")
        val vote = Vote(10, "12345678911", VoteResponse.YES, guideline)

        val exception = shouldThrow<CPFCanVoteValidation.InvalidCPFException> {
            cpfCanVoteValidation.isNotValid(vote)
        }
        exception.message shouldBe "Use a valid cpf: 12345678911"
    }

    @Test
    fun `it should be not valid when response is 'UNABLE_TO_VOTE'`() {
        every { client.executeRequest(any()).statusCode } returns 200
        every { client.executeRequest(any()).responseMessage } returns "OK"
        every { client.executeRequest(any()).dataStream } returns UNABLE_TO_VOTE.byteInputStream()

        val guideline = Guideline(1, "Guideline")
        val vote = Vote(10, "12345678911", VoteResponse.YES, guideline)

        val notValid = cpfCanVoteValidation.isNotValid(vote)

        notValid shouldBe true
    }

    @Test
    fun `it should be valid when response is 'ABLE_TO_VOTE'`() {
        every { client.executeRequest(any()).statusCode } returns 200
        every { client.executeRequest(any()).responseMessage } returns "OK"
        every { client.executeRequest(any()).dataStream } returns ABLE_TO_VOTE.byteInputStream()

        val guideline = Guideline(1, "Guideline")
        val vote = Vote(10, "12345678911", VoteResponse.YES, guideline)

        val notValid = cpfCanVoteValidation.isNotValid(vote)

        notValid shouldBe false
    }

    @Test
    fun `it should return a message saing that the cpf is not able to vote`() {
        val guideline = Guideline(1, "Guideline")
        val vote = Vote(10, "12345678911", VoteResponse.YES, guideline)

        val message = cpfCanVoteValidation.message(vote)

        message shouldBe "This cpf ${vote.cpf} is not able to vote!"
    }
}

const val UNABLE_TO_VOTE = """
        {
            "status": "UNABLE_TO_VOTE"
        }
    """

const val ABLE_TO_VOTE = """
        {
            "status": "ABLE_TO_VOTE"
        }
    """