package org.bruno.vote.session.service


import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import io.mockk.*
import org.bruno.vote.session.model.Guideline
import org.bruno.vote.session.repository.GuidelineRepository
import org.bruno.vote.session.service.vote.VoteBroadcastService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import reactor.core.Disposables
import java.util.*

internal class GuidelineServiceTest {
    private val guidelineRepository = mockk<GuidelineRepository>()
    private val voteBroadcastService = mockk<VoteBroadcastService>()

    private lateinit var guidelineService: GuidelineService

    @BeforeEach
    fun setup() {
        guidelineService = GuidelineService(guidelineRepository, listOf(voteBroadcastService))
    }

    @Test
    fun `should save a new guideline`() {
        val guideline = Guideline(description = "guideline")

        every { guidelineRepository.save(guideline) } returns Guideline(1, "guideline")

        val newGuideline = guidelineService.save(guideline)

        newGuideline shouldBe Guideline(1, "guideline")
    }

    @Test
    fun `should open a new vote session`() {
        val guideline = Guideline(1, "guideline")
        every { guidelineRepository.findByIdAndActive(any(), any()) } returns guideline
        every { guidelineRepository.save(any<Guideline>()) } returns guideline
        val openedGuideline = guidelineService.openVotes(1, 5)

        openedGuideline shouldBe Guideline(id = 1, description = "guideline", active = true)
    }

    @Test
    fun `should not open an new vote when there is no guideline`() {
        every { guidelineRepository.findByIdAndActive(any(), any()) } returns null

        val exception = shouldThrow<GuidelineNotFoundException> {
            guidelineService.openVotes(1, 5)
        }
        exception.message shouldBe "The guideline 1 was not found!"
    }

    @Test
    fun `it should close and broadcast the guideline`() {
        val guideline = Guideline(1, "guideline", true)
        every { guidelineRepository.findById(any()) } returns Optional.of(guideline)
        every { guidelineRepository.save(any<Guideline>()) } returns guideline
        every { voteBroadcastService.send(any()) } just runs

        guidelineService.close(guideline.id!!)

        verify { guidelineRepository.save(any<Guideline>()) }
        verify { voteBroadcastService.send(any()) }
    }

    @Test
    fun `it should throw a GuidelineNotFoundException while loading a not exists guideline`() {
        every { guidelineRepository.findById(any()) } returns Optional.empty()

        val exception = shouldThrow<GuidelineNotFoundException> {
            guidelineService.close(1)
        }

        exception.message shouldBe "The guideline 1 was not found!"

        verify(exactly = 0) { guidelineRepository.save(any<Guideline>()) }
        verify(exactly = 0) { voteBroadcastService.send(any()) }
    }

}