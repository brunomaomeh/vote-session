package org.bruno.vote.session.service.vote

import io.kotlintest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import org.bruno.vote.session.model.Guideline
import org.bruno.vote.session.model.Vote
import org.bruno.vote.session.model.VoteResponse
import org.bruno.vote.session.repository.VoteRepository
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class UniqueVoteValidationTest {

    private val voteRepository = mockk<VoteRepository>()

    private lateinit var uniqueVoteValidation: UniqueVoteValidation

    @BeforeEach
    fun setup() {
        uniqueVoteValidation = UniqueVoteValidation(voteRepository)
    }

    @Test
    fun `it should not be valid if there is another vote with same cpf`() {
        val guideline = Guideline(1, "guideline")
        val vote = Vote(10, "000", VoteResponse.YES, guideline)
        val anotherVote = Vote(20, "000", VoteResponse.NO, guideline)
        every { voteRepository.findByCpfAndGuideline(any(), vote.guideline) } returns listOf(anotherVote)
        val notValid = uniqueVoteValidation.isNotValid(vote)

        notValid shouldBe true
    }

    @Test
    fun `it should be valid if there isn't another vote with same cpf`() {
        val guideline = Guideline(1, "guideline")
        val vote = Vote(10, "000", VoteResponse.YES, guideline)
        every { voteRepository.findByCpfAndGuideline(any(), vote.guideline) } returns emptyList()
        val notValid = uniqueVoteValidation.isNotValid(vote)

        notValid shouldBe false
    }


    @Test
    fun `it should return a message saing that the cpf is not able to vote`() {
        val guideline = Guideline(1, "Guideline")
        val vote = Vote(10, "12345678911", VoteResponse.YES, guideline)

        val message = uniqueVoteValidation.message(vote)

        message shouldBe "This cpf ${vote.cpf} already voted"
    }


}