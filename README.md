# vote-session
 Este projeto visa executar o Desafio Técnico da Sicredi - Sistema de Votação

## Dependencias
O projeto tem como dependencia o `PostgreSQL` e o `ActiveMQ`. Ambos estão configurados no `docker-compose`, bastando executá-lo com o seguinte comando

```
#!shell
docker-compose up
```
Isto já é o suficiente para ter todas as dependencias sendo executadas. 
Vale lembrar que todas as dependencias devem estar sendo executadas no momento do startup da aplicação.

## Gradle
O projeto foi construido usando o gradle como gerenciador de build. Para executar o projeto basta executar o comando:

```
#!shell
./gradlew clean bootRun
```
O gradle irá se responsabilizar por baixar todas as dependencias necessárias para executar o projeto, inclusive ele próprio.


##### IDE
O gradle também pode ser reponsável por gerenciar as configurações de cada IDE, para isso execute:

```
#!shell
./gradlew idea
# para configurar para o intellij idea ou
./gradlew eclipse
# para configurar para o eclipse
```

## Linguagem
O projeto foi desenvolvido usando a linguagem Kotlin, que deve ser compilado para o Java8*, junto com o Spring boot.

Por um problema de integração do gradle com o intellij, a configuração da versão do Java deve ser manual, seguindo os passos no intellij idea:

```
 File | Settings | Build, Execution, Deployment | Compiler | Kotlin Compiler | Target JVM Version -> selecionar 1.8
```

## Docker Compose
Para baixar o docker-compose, siga as instruções no site: https://docs.docker.com/compose/install/

## Metodos HTTP
- Cadastrar nova pauta:
```
#!shell
curl -X POST -H "Content-Type: application/json" --data '{ "description": "Guide01" }' localhost:8080/guidelines
```

- Abrir sessão de votação:
```
#!shell
curl -X PUT -H "Content-Type: application/json" --data '{ "timeout": 30 }' localhost:8080/guidelines/{ID}
# ou para votar com o tempo default:
curl -X PUT -H "Content-Type: application/json" --data '{ }' localhost:8080/guidelines/{ID}
```

- Votar em uma pauta:
```
#!shell
curl -X POST -H "Content-Type: application/json" --data '{ "vote": "[YES|NO]", "cpf": "12345678910" }' localhost:8080/guidelines/{ID}/votes
```

- Visualizar os votos contabilizados:
```
#!shell
curl -X GET -H "Content-Type: application/json" localhost:8080/guidelines/{ID}/votes
```

_Sempre lembrando de trocar o {ID} pelo ID da pauta cadastrada no primeiro método descrito._

## Mensageria
Foi escolhido o `ActiveMQ` por sua facilidade de uso e configuração.

Existem 2 classes que manipulam os dados junto do `ActiveMQ`: 

    VoteBroadcastService: responsável por publicar o resultado na votação em uma fila.
    ActiveMQConsumer: responsável por consumir a lista de resultado, apenas faz o log do resultado.
    
Para gerenciar a fila de votação, é possível acessá-la pela url: http://0.0.0.0:8161/admin/queues.jsp

    User: admin
    Pass: admin
    
## Persistencia
A persistencia do projeto é feita em uma base de dados `PostgreSQL`, a sua escolha foi feita por ser um banco de dados padrão de mercado, visto também que o problema não exige uma forma de persistencia de dados muito diferente disso.

Para acessar os dados, basta criar uma conexão sql:

    Host: localhost
    Port: 5432
    Database: postgres
    User: postgres
    Pass: postgres
    
## Documentação de API
A documentação da API é feita pelo `Swagger`. Apenas foi adicionado uma documentação simples de cada operação realizada dentro do sistema.

Para acessar a documentação gerada pelo `Swagger`, basta acessar a url: http://localhost:8080/swagger-ui.html
    
## Tarefas bônus
#### Tarefa Bônus 1 - Integração com sistemas externos
Antes de salvar um voto, o sistema validará se o CPF informado é valido.
 Esta validação é feita na classe `CPFCanVoteValidation`.
 
### Tarefa Bônus 2 - Mensageria e filas
Após uma votação ser aberta, é colocado um timeout (em segundos) para avisar o restante da plataforma quando a votação acabar.
Ao encerrar uma votação, uma menssagem é disparada para uma fila do `ActiveMQ` pela classe `ActiveMQBroadcastService`.

### Tarefa Bônus 4 - Versionamento da API
Uma forma de versionar uma API é prefixar as rotas da API com o número da versão, exemplo: http://sicredi.com.br/v2/guideline. Acredito que essa seja uma forma bem simples e efetiva de resolver diferentes versões da API.

## Explicação sobre algumas decisões
  
Foi escolhido a linguagem Kotlin para o desenvolvimento deste problema pois é uma linguagem que venho estudando há algum tempo, e vi a oportunidade de praticá-la um pouco mais em uma aplicação não critica, além da interoperabilidade completa que ela tem com a Linguagem Java, sendo possivel usar frameworks bastante conhecidos no mundo java, como o Spring e o JPA/Hibernate. A escolha pelos frameworks Spring e JPA/Hibernate se deu pelo o conhecimento que eu possuo sobre eles, vindo da minha carreira como desenvolvedor Java. Também foi escolhidos outros frameworks menores que são puramente Kotlin, como o Fuel (para requisições http) e o Mockk (para mocks em testes unitários).

Esta é uma aplicação REST feita usando framework Spring boot, usando toda a sua convenção padrão. Existem 2 classes principais no projeto: Guideline (que representa uma pauta) e Vote (que representa o voto de um associado à uma determinada pauta). Possuindo um controlador que faz o mapeamento de todas as rotas necessárias para o uso do sistema. 

A camada de serviço é responsável por decidir quais ações tomar, seja ela uma validação e/ou persistir no banco de dados, por exemplo. A interface VoteValidation, que é injetada como uma lista dentro do objeto VoteService, facilita a implementação de novos validadores.

Foi utilizado a interface CrudRepository do Spring na camada de persistencia. Como também foi ativado para que o JPA/Hibernate construa e atualize a estrutura do banco de dados, a fim de uma velocidade maior na construção inicial do projeto. Em um projeto real ferramentas de migração de banco de dados, como Liquibase ou Flyway, se fazem necessária. 

Uma grande dificuldade que eu tive foi de reagir ao tempo que uma Pauta deveria ficar aberta para poder fechá-la e enviá-la ao sistema de mensageria. Até pensar um pouco mais sobre sistemas reativos, eu lembrei que era possível dar um delay na execução do elemento. A solução foi feita, mas ainda tenho muito a aprender sobre sistemas reativos, inclusive formas de testá-los.

Por fim, foi criado um consumer apenas para a verificação do consumo da fila de mensageria que só faz enviar a mensagem recebida para o log do sistema. 